package io.gitlab.donespeak.entity;

import lombok.Data;

@Data
public class User {
    private long userId;
    private String name;
    private String email;
}
