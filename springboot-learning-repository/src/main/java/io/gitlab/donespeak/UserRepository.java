package io.gitlab.donespeak;

import io.gitlab.donespeak.entity.User;

public interface UserRepository {

    User getByUserId(long userId);
}
