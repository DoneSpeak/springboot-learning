package io.gitlab.donespeak.controller;

import io.gitlab.donespeak.UserService;
import io.gitlab.donespeak.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{userId:\\d+}")
    public User getUser(@PathVariable long userId) {

        return userService.getUserById(userId);
    }
}
