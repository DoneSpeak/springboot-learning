package io.gitlab.donespeak.config;

import io.gitlab.donespeak.UserRepository;
import io.gitlab.donespeak.entity.User;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaseConfig {

    @Bean
    public UserRepository UserRepository() {

        return new UserRepository() {

            @Override
            public User getByUserId(long userId) {
                User user = new User();
                user.setUserId(userId);
                user.setEmail("guanrong.yang@foxmail.com");
                user.setName("DoneSpeak");

                return user;
            }
        };
    }
}
