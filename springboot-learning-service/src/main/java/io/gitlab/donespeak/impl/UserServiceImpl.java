package io.gitlab.donespeak.impl;

import io.gitlab.donespeak.UserRepository;
import io.gitlab.donespeak.UserService;
import io.gitlab.donespeak.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User getUserById(long userId) {
        return userRepository.getByUserId(userId);
    }
}
