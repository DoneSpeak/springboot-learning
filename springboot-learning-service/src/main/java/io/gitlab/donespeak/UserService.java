package io.gitlab.donespeak;

import io.gitlab.donespeak.entity.User;

public interface UserService {

    User getUserById(long userId);
}
